'''
Created on Jun 16, 2010

@author: nicholas
'''
import re #regular expression module
import os
import locale #so we can get the system's preferred encoding scheme
import random
import shlex, subprocess
from optparse import OptionParser
import glob
import shutil
import sys

class BibMerger:
    
    def __init__(self):
        self.bibDictionary = {} #this will be key = bib key we use in latex, and the rest whole text for a key (including the key) as the value
        
        
    #given a specific path, open each file in said path and store the unique keys and values into the dictionary
    def readInAllFiles(self, path):
        files = []
        for eachFile in os.listdir(path): #get all the bib files
            if os.path.isfile(path + "/" + eachFile) and ".bib" in eachFile:
                files.append(eachFile)
        
        files.sort()
        
        for eachFile in files: #go through each file
            text = open(path+"/"+eachFile).readlines()
            
            
            
            key = ""
            value = ""
            braceCounter = 0
            for eachLine in text:
                if eachLine.startswith("%") or eachLine.startswith("\n"):
                    continue
                
                if "@" in eachLine:
                    if braceCounter != 0:
                        sys.exit("somehow our brace counter isnt 0 when we found a new key")
                    
                    value = eachLine
                    key = re.sub(",", "", re.split("{", eachLine)[1]) #this gets us just the key and removes any commas that are there
                    braceCounter += eachLine.count("{")
                
                else:
                    value += eachLine
                    braceCounter += eachLine.count("{")
                    braceCounter -= eachLine.count("}")
                    
                if braceCounter == 0:
                    if not key in self.bibDictionary: #if the key isnt in there, put it in, if it is...just ignore it
                        self.bibDictionary[key] = value
                        
        
    def outputResults(self):
        keys = self.bibDictionary.keys()
        output = open(os.getcwd() + "/results.bib", 'w')
        
        for eachKey in keys:
            output.writelines(self.bibDictionary[eachKey])
        
        
        output.close()
                
            

if __name__ == "__main__":
    filesPath = sys.argv[1]
    
    b = BibMerger()
    b.readInAllFiles(filesPath)
    b.outputResults()
    